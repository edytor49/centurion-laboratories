# Centurion laboratories #

### Для чего данный репозиторий? ###

* Centurion laboratories
* Version: 1.0.0
* Author: edytor49 (edytor49@gmail.com)

### Используемые инструменты: ###

* Yarn - менеджер зависимотей (Подробнее: https://yarnpkg.com/en/)
* Gulp - сборщик проекта
* Bower - пакетный менеджер, установка vendor скриптов, библиотек и т.д. (https://bower.io/)
* Sass - препроцессор CSS
* Pug (jade) - препроцессор HTML и шаблонизатор (https://pugjs.org/api/getting-started.html) (https://habrahabr.ru/post/278109/)
* Compass (or Bourbon) -- огромная библиотека кросбраузерных миксинов и не только

### Используемые основные принципы: ###

* BEM (только подход к наименованию классов)
* Архитектура sass стилей проекта, смесь SMACSS (Шаблон 7-1) Гайдлайн который поможет разобратся в структуре проекта тут: http://sass-guidelin.es/
* Framework: Bootstrap 3 (http://getbootstrap.com/) or UiKit 2 (https://getuikit.com/v2/)
* Flexbox

### Инструкция по инициализации проекта: ###

* Скачать последнюю версию template из Bitbucket используя для навигации теги
* Скопировать содержимое в корень проекта
* Установить зависимости проекта для разработки через пакетный менеджер yarn
* yarn install (or yarn) в консоли
* yarn outdated -- проверка обновлений для используемых зависимостей
* yarn upgrade -- обновление используемых зависимостей к последней актуальной версии
* bower init -- для создания bower.json
* bower install -- установка vendor скриптов от которых зависит проект (к примеру: jQuery)
* gulp build -- запуск разработки проэкта
* gulp -- запуск сборщика проекта, а также watcher

### Примечание ###

* Персонализировать под проект favicon -- в директории app/image/favicon/

### Полезные ссылки ###

* https://habrahabr.ru/post/243335/ -- Все о npm, пакетах, утсановке пакетов
