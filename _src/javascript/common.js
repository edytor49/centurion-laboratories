$(function() {
  // Toogle Navigation (Mobile Menu)
  $(".js-toggle-nav").click(function(e) {
      var target = $(this).data('target');
      $(this).toggleClass('is-open'); // add to button active class

      $(target).toggleClass('is-open'); // show menu

      e.preventDefault();
  });
});

$(document).ready(function() {
  // #Slider
  $('#slider .owl-carousel').owlCarousel({
    loop: true,
    margin: 0,
    nav: true,
    items: 1,
    // animateOut:"fadeOut",
    animateIn: 'fadeIn',
    navContainer: '#slider .slider__nav',
    navText: ['<i class="icon icon-arrow-left"></i>','<i class="icon icon-arrow-right"></i>']
  });

  // #Featured Carousel
  $('#featured .owl-carousel').owlCarousel({
    loop: false,
    margin: 30,
    nav: true,
    dots: false,
    items: 1,
    // animateOut:"fadeOut",
    // animateIn: 'fadeIn',
    navContainer: '#featured .carousel__nav',
    navText: ['<i class="icon icon-arrow-left"></i>','<i class="icon icon-arrow-right"></i>']
  });

  // #Parthers Carousel
  $('#partners .owl-carousel').owlCarousel({
    loop: false,
    margin: 30,
    dots: true,
    responsive : {
        0 : {
          items:1,
          nav:false
        },
        480 : {
          items:2,
          nav:false
        },
        768 : {
          items:3,
          nav:true
        }
    },
    // animateOut:"fadeOut",
    // animateIn: 'fadeIn',
    navContainer: '#partners .carousel__nav',
    navText: ['<i class="icon icon-arrow-left"></i>','<i class="icon icon-arrow-right"></i>']
  });
});
