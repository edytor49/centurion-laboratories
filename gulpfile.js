global.hostname = "localhost";

var gulp = require('gulp'),
  gutil = require('gulp-util'),
  sass = require('gulp-sass'),
  // compass        = require('gulp-compass'),
  bourbon = require('node-bourbon'),
  browserSync = require('browser-sync'),
  sourcemaps = require('gulp-sourcemaps'),
  concat = require('gulp-concat'), // обьеденяет скрипты в один файл
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename'), // переименование файлов после исполнения таска
  del = require('del'),
  cache = require('gulp-cache'),
  autoprefixer = require('gulp-autoprefixer'),
  newer = require('gulp-newer'), // компилируем лишь изменённые файлы (а не все)
  notify = require("gulp-notify") // вывод ошибок в консоль
  imagemin = require('gulp-imagemin'),
  cleancss = require('gulp-clean-css'),
  spritesmith = require('gulp.spritesmith'),
  gulpif = require('gulp-if'),
  pug = require('gulp-pug');

// Static Server + watching scss/html files
gulp.task('browser-sync', function() {
  browserSync({
    server: {
      baseDir: "./app"
    },
    notify: false,
    // tunnel: true,
    // tunnel: "projectmane", //Demonstration page: http://projectmane.localtunnel.me
  });
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
  return gulp.src("_src/stylesheet/sass/**/*.scss")
    .pipe(sourcemaps.init()) //Инициализируем sourcemap

    // # Compass -- remove comment if u need it
    // .pipe(compass({ // пройдемся через compass
    //   config_file: './config.rb',
    //   css: 'src/stylesheet/css/',
    //   sass: 'src/stylesheet/sass',
    // 	image: 'src/stylesheet/images',
    // 	font: 'app/fonts'
    // })).on('error', function(error) {
    //   console.log(error);
    // })

    // # Bourbon -- comment if u use Compass
    .pipe(sass({
      includePaths: bourbon.includePaths
    }).on("error", notify.onError()))
    .pipe(newer('_src/stylesheet/sass/')) // обрабатывать только новые или изменённые файлы
    .pipe(autoprefixer(['last 15 versions'])) // Добавим вендорные префиксы
    //  .pipe(gulp.dest("app/css")) // save new css file NOT MINIFIED
    .pipe(cleancss({
      debug: true
    }, function(details) { // а теперь "ужмём" стили
      console.log(details.name + ': ' + details.stats.originalSize);
      console.log(details.name + ': ' + details.stats.minifiedSize);
    })) // minify css
    .pipe(rename({
      suffix: '.min',
      prefix: ''
    })) // rename the css file
    .pipe(sourcemaps.write()) // save sourcemaps
    .pipe(gulp.dest("app/stylesheet")) // save new css file MINIFIED
    .pipe(browserSync.reload({
      stream: true
    })); // reload page
});

// Compile HTML (Rigger)
gulp.task('html', function() {
  return gulp.src("_src/*.html") //Выберем файлы по нужному пути
    .pipe(rigger()) // Прогоним через rigger
    .pipe(gulp.dest("app/")) // сохраним новый html файл
    .pipe(browserSync.stream()); // reload page
});

// Compile Pug (Jade) -> HTML
gulp.task('pug', function() {
  return gulp.src('_src/template/pages/*.pug')
    .pipe(pug({
      pretty: true
    })).on("error", notify.onError()) // pip to jade plugin
    .pipe(gulp.dest('app/')); // tell gulp our output folder
});

// Таск на выполение действий при изменении файлов
gulp.task('watch', ['sass', 'pug', 'scripts', 'browser-sync'], function() {
  gulp.watch("_src/template/**/*.pug", ['pug']);
  gulp.watch("_src/stylesheet/sass/**/*.scss", ['sass']);
  gulp.watch('app/*.html', browserSync.reload);
});

// Scripts concat
gulp.task('scripts', function() {
  return gulp.src([ // тут вручную перечислить все скрипты которые необходимо обьеденить
      '_src/vendor/jquery/dist/jquery.min.js',
      '_src/vendor/owl.carousel/dist/owl.carousel.min.js',
      // '_src/vendor/bootstrap/dist/js/bootstrap.min.js',
      // '_src/vendor/swiper/dist/js/swiper.jquery.min.js',
      // '_src/vendor/readmore-js/readmore.min.js',
      // '_src/vendor/slideout-js/dist/slideout.min.js',
      // '_src/vendor/scrollpos-styler/scrollPosStyler.js',
      '_src/javascript/common.js' // Всегда в конце
    ])
    .pipe(concat('scripts.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('app/javascript'))
    .pipe(browserSync.reload({
      stream: true
    }));
});

// Images
gulp.task('images', function() {
  return gulp.src("_src/image/data/**/*") // Выберем все фото
    .pipe(cache(imagemin()))
    .pipe(gulp.dest("app/image/data")) // сохраним
});

// Stylesheet Images
gulp.task('stylesheet-images', function() {
  return gulp.src("_src/stylesheet/images/**/*") // Выберем все фото
    // .pipe(cache(imagemin()))
    .pipe(gulp.dest("_src/stylesheet/")) // сохраним
});

// Sprite
gulp.task('sprites', function() {
  var spriteData = gulp.src('_src/stylesheet/images/png-icons/*.png').pipe(spritesmith({
    imgName: 'icons-sprite.png',
    cssName: '_icons-sprite.scss',
    imgPath: 'images/png-icons/icons-sprite.png'
  }));

  var imgStream = spriteData.img
    .pipe(gulp.dest('app/stylesheet/images/png-icons/'));

  var cssStream = spriteData.css
    .pipe(gulp.dest('_src/stylesheet/sass/0-vendors/'));
});

// Build project app/
gulp.task('build', ['removedist', 'images', 'sass', 'pug', 'scripts'], function() {

  var buildFiles = gulp.src([
    '_src/*.html',
    '_src/.htaccess',
  ]).pipe(gulp.dest('app'));

  var buildCss = gulp.src([
    '_src/stylesheet/css/stylesheet.min.css',
  ]).pipe(gulp.dest('app/stylesheet'));

  var buildCssImages = gulp.src([
    '_src/stylesheet/images/**/*',
  ]).pipe(gulp.dest('app/stylesheet/images'));

  // var buildJs = gulp.src([
  // 	'_src/javascript/scripts.min.js',
  // ]).pipe(gulp.dest('app/javascript'));

  var buildFonts = gulp.src([
    '_src/fonts/**/*',
  ]).pipe(gulp.dest('app/stylesheet/fonts'));

  // var buildImage = gulp.src([
  // 	'_src/image/**/*',
  // ]).pipe(gulp.dest('app/image'));

});

// Deploy to FTP server
gulp.task('deploy', function() {

  var conn = ftp.create({
    host: 'hostname.com',
    user: 'username',
    password: 'userpassword',
    parallel: 10,
    log: gutil.log
  });

  var globs = [
    'dist/**',
    'dist/.htaccess',
  ];
  return gulp.src(globs, {
      buffer: false
    })
    .pipe(conn.dest('/path/to/folder/on/server'));

});

gulp.task('removedist', function() {
  return del.sync('app');
});
gulp.task('clearcache', function() {
  return cache.clearAll();
});

gulp.task('default', ['watch']);
